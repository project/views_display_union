## Views Display Union

This module adds a Views display that uses a SQL `UNION` to combine one or more
results sets with the results of a master display. This is useful when you need
to logically OR two or more sets of results that have different contextual
filters or use different relationships.

- Project homepage:  https://www.drupal.org/project/views_display_union
- Tutorial:  https://www.drupal.org/docs/contributed-modules/views-display-union
